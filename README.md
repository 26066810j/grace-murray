# Grace Murray
![This is an alt text.](grace_hopper.jpg "Foto Jubilació")
## Biografia
*Grace Hopper va ser una militar nord-americana, amb grau d'almirall i una autèntica pionera en el món de la informàtica. Va ser la primera programadora que va utilitzar el Mark I i també la inventora del concepte de compilador d'un llenguatge de programació.*

## Dades personals

| | |
| ------------- |:-------------:|
|**Naixement:** | 9 de desembre de 1906, Nova York, Nova York, [Estats Units.](https://www.google.com/search?q=estados+unidos&oq=estados+unidos&aqs=chrome.0.0i131i355i433i512j46i131i433i512j0i512l3j46i512j0i512l4.5006j0j7&sourceid=chrome&ie=UTF-8&safe=active&ssui=on)|
| **Defunció:** | 1 de gener de 1992, Comtat d'Arlington, Virgínia, [Estats Units.](https://www.google.com/search?q=estados+unidos&oq=estados+unidos&aqs=chrome.0.0i131i355i433i512j46i131i433i512j0i512l3j46i512j0i512l4.5006j0j7&sourceid=chrome&ie=UTF-8&safe=active&ssui=on)|
|**Cònjuge:** |Vincent Foster Hopper (c. 1930–1945) |
| **Pares:** | Mary Campbell van Horne, Walter Fletcher Murray | 
| **Premis:**| Medalla Nacional de Tecnologia i Innovació, Medalla Presidencial de la Llibertat, Premi Emanuel R. Piore de l'IEEE | 

## Historia
> Grace Hooper va ser una de les dones més influents en l'àmbit de la informàtica, a part de realitzar una vida dedicada a la seva pàtria (Va enfocar els seus estudis a millorar la tecnologia nord-americana i, conseqüentment, mundial). El desenvolupament de la primera màquina calculadora electromecànica (Mark I, junt amb Howard Aiken) i la creació del llenguatge COBOL, el qual permetia que gent poc especialitzada pogués programar, va generar una base d'ajuda i de simplificació perquè el món de la informàtica evolucionés.
>### Vida i Educació
>>Hopper va néixer a la ciutat de Nova York. Ella era la gran d'una família de tres fills. Els seus pares, Walter Fletcher Murray i Mary Campbell Van Horne, eren d'origen holandès i escocès, i van assistir a West End Collegiate Church. El seu besavi, Alexander Wilson Russell, un almirall de l'Armada dels Estats Units, va lluitar a la Batalla de la badia Mobile durant la Guerra Civil dels Estats Units.
>### Jubilació
>>Hopper es va retirar de la Reserva Naval als 60 anys, d'acord amb les regulacions de desgast de l'Armada, amb el rang de Comandant al final de 1966. Va ser cridada al servei actiu a l'agost de 1967 per a un període de sis mesos que es va convertir en una missió indefinida. Es va retirar novament el 1971, però se li va demanar de tornar al servei actiu de nou en 1972. Va ser ascendida a capitana el 1973 per l'almirall Elmo R. Zumwalt Jr.
>>>![This is an alt text.](gracehopper_2.JPEG "Foto Jubilació")

## Inspiracións
Les persones que la ven inspirar van ser: `John Mauchly, Howard H. Aiken i Richard Courant`